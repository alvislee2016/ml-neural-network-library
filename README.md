# Machine Learning Neural Network Library
A neural network library from scratch using C++ Eigen libray for linear algebra computation.

## Setup
Create build directory
```bash
mkdir build && cd $_
```
Create MAKEFILE using CMAKE
```bash
cmake ..
```
Create executable using MAKEFILE
```bash
make
```
Run executable
```
./main
```

## Layers
* Linear (Dense) Layer
* Sigmoid Layer
* Softmax Layer

## Regularizers
* L1 Regularization
* L2 Regularization
* L1L2 Regularization

## Loss
* Mean Squared Error
* Binary Cross Entropy
* Categorical Cross Entropy

## Optimizers
* Standard Gradient Descent
    * Momentum
    * Nesterov Momentum
* AdaGrad
* RMSprop

## Metrics
* Mean Squared Error
* R Squared
* Accuracy

## Data
* Classification.csv: Contains data of 4 clusters centered at (2,2), (-2,-2), (2,-2) and (-2,2)