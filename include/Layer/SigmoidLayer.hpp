#include "Layer.hpp"

#ifndef __SIGMOID_HPP__
#define __SIGMOID_HPP__

namespace NNLibrary {
    class SigmoidLayer : public Layer {
        private:
            static double sigmoid(double x);
        public:
            SigmoidLayer();
            ~SigmoidLayer();
            Eigen::MatrixXd forward(Eigen::MatrixXd X);
            Eigen::MatrixXd backward(Eigen::MatrixXd gradZ);
    };
};

#endif