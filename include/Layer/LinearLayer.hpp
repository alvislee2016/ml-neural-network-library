#include "Layer.hpp"

#ifndef __LINEAR_HPP__
#define __LINEAR_HPP__

namespace NNLibrary {
    class LinearLayer : public Layer {
        private:
            Eigen::MatrixXd W;
            Eigen::VectorXd b;
            double l1;
            double l2;
        public:
            LinearLayer(int M, int N, double l1=0.0, double l2=0.0);
            ~LinearLayer();
            Eigen::MatrixXd gradW;
            Eigen::VectorXd gradb;
            Eigen::ArrayXXd cachedGradW;
            Eigen::ArrayXd cachedGradb;
            Eigen::MatrixXd forward(Eigen::MatrixXd X);
            Eigen::MatrixXd backward(Eigen::MatrixXd gradZ);
            void update(double lr, Eigen::MatrixXd gradW, Eigen::VectorXd gradb);
    };
};

#endif