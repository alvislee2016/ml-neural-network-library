#include <Eigen/Dense>

#ifndef __LAYER_HPP__
#define __LAYER_HPP__

namespace NNLibrary {
    class Layer {
        protected:
            Eigen::MatrixXd cachedX;
        public:
            virtual ~Layer() {}
            virtual Eigen::MatrixXd forward(Eigen::MatrixXd X) = 0;
            virtual Eigen::MatrixXd backward(Eigen::MatrixXd gradZ) = 0;
    };
};

#endif