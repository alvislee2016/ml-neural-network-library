#include "Layer.hpp"

#ifndef __SOFTMAX_HPP__
#define __SOFTMAX_HPP__

namespace NNLibrary {
    class SoftmaxLayer : public Layer {
        public:
            SoftmaxLayer();
            ~SoftmaxLayer();
            Eigen::MatrixXd forward(Eigen::MatrixXd X);
            Eigen::MatrixXd backward(Eigen::MatrixXd gradZ);
    };
};

#endif