#include <iostream>
#include <Eigen/Dense>

class CSVReader {
    private:
        std::string fileName;
        char delimeter;
    public:
        CSVReader(std::string fileName, char delimeter);
        ~CSVReader();
        Eigen::MatrixXd getData(int rows, int cols);
};