#include <list>
#include "Layer/Layer.hpp"
#include "Optimizer/Optimizer.hpp"
#include "Metrics.hpp"

#ifndef __SEQUENTIAL_HPP__
#define __SEQUENTIAL_HPP__

namespace NNLibrary {
    class Sequential {
        private:
            std::list<Layer*> layers;
            Eigen::MatrixXd (*loss)(Eigen::MatrixXd Yhat, Eigen::MatrixXd Y);
            Optimizer* optimizer;
            Metric* metric;
            Eigen::MatrixXd forward(Eigen::MatrixXd X);
            void backward(Eigen::MatrixXd Y);
            void update();
        public:
            Sequential();
            ~Sequential();
            void add(Layer* layer);
            void compileLoss(Eigen::MatrixXd (*loss)(Eigen::MatrixXd, Eigen::MatrixXd));
            void compileOptimizer(Optimizer* optimizer);
            void compileMetric(Metric* metric);
            void fit(Eigen::MatrixXd X, Eigen::MatrixXd Y, int epochs, int batchSize);
            Eigen::MatrixXd predict(Eigen::MatrixXd X);
    };
}

#endif