#include <Eigen/Dense>

#ifndef __PREPROCESS_HPP__
#define __PREPROCESS_HPP__

namespace NNLibrary {
    class Preprocess {
        public:
            Preprocess();
            ~Preprocess();
            static void minmaxNormalize(Eigen::MatrixXd& data);
            static void shuffle(Eigen::MatrixXd& data);
            static Eigen::MatrixXd onehotEncoding(Eigen::VectorXd Y, int K);
    };
};

#endif