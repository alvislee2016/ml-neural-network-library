#include <Eigen/Dense>

#ifndef __LOSS_HPP__
#define __LOSS_HPP__

namespace NNLibrary {
    class Loss {
        public:
            Loss();
            ~Loss();
            static Eigen::MatrixXd meanSquaredError(Eigen::MatrixXd Yhat, Eigen::MatrixXd Y);
            static Eigen::MatrixXd binaryCrossEntropy(Eigen::MatrixXd Yhat, Eigen::MatrixXd Y);
            static Eigen::MatrixXd categoricalCrossEntropy(Eigen::MatrixXd Yhat, Eigen::MatrixXd Y);
    };
};

#endif