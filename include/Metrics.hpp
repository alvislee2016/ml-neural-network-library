#include <Eigen/Dense>

#ifndef __METRICS_HPP__
#define __METRICS_HPP__

namespace NNLibrary {
    
    typedef double (*Score)(Eigen::MatrixXd Yhat, Eigen::MatrixXd Y);
    struct Metric { std::string name; Score score; };
    
    class Metrics {
        public:
            Metrics();
            ~Metrics();
            static Metric meanSquaredError;
            static Metric rSquared;
            static Metric accuracy;
    };
};

#endif