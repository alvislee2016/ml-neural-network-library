#include "Optimizer.hpp"

#ifndef __SGD_HPP__
#define __SGD_HPP__

namespace NNLibrary {
    class SGD : public Optimizer {
        private:
            double lr;
            double momentum;
            bool nesterov;
        public:
            SGD(double lr=0.001, double momentum=0.0, bool nesterov=false);
            ~SGD();
            void optimize(LinearLayer* layer);
    };
};

#endif