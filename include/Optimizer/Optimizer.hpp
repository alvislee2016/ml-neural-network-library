#include <list>
#include "Layer/Layer.hpp"
#include "Layer/LinearLayer.hpp"

#ifndef __OPTIMIZER_HPP__
#define __OPTIMIZER_HPP__

namespace NNLibrary {
    class Optimizer {
        public:
            virtual ~Optimizer() {}
            virtual void optimize(LinearLayer* layer) = 0;
    };
};

#endif