#include "Optimizer.hpp"

#ifndef __RMSPROPS_HPP__
#define __RMSPROPS_HPP__

namespace NNLibrary {
    class RMSprop : public Optimizer {
        private:
            double lr;
            double rho;
            static const double epsilon;
        public:
            RMSprop(double lr=0.01, double rho=0.9);
            ~RMSprop();
            void optimize(LinearLayer* layer);
    };
};

#endif