#include "Optimizer.hpp"

#ifndef __ADAGRAD_HPP__
#define __ADAGRAD_HPP__

namespace NNLibrary {
    class AdaGrad : public Optimizer {
        private:
            double lr;
            static const double epsilon;
        public:
            AdaGrad(double lr=0.1);
            ~AdaGrad();
            void optimize(LinearLayer* layer);
    };
};

#endif