#include <fstream>
#include "CSVReader.hpp"

using namespace std;
using namespace Eigen;

CSVReader::CSVReader(string fileName, char delimeter) :
    fileName(fileName), delimeter(delimeter) {}

CSVReader::~CSVReader() {}

MatrixXd CSVReader::getData(int rows, int cols) {
    
    ifstream in(fileName);
    string line;

    int row = 0;
    int col = 0;

    MatrixXd data(rows, cols);
    if(in.is_open()) {
        while(getline(in, line)) {
            col = 0;
            
            char* ptr = (char *) line.c_str();
            char* current = ptr; 

            for(int i = 0; i < line.length(); i++) {
                if(ptr[i] == delimeter) {
                    data(row, col++) = atof(current);
                    current = ptr + i + 1;
                }
            }
            data(row, col) = atof(current);
            
            row++;
        }

        in.close();
    }

    return data;
}