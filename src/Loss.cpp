#include "Loss.hpp"

using namespace Eigen;
using namespace NNLibrary;

Loss::Loss() {}
Loss::~Loss() {}

MatrixXd Loss::meanSquaredError(MatrixXd Yhat, MatrixXd Y) {
    return Yhat - Y;
}

MatrixXd Loss::binaryCrossEntropy(MatrixXd Yhat, MatrixXd Y) {
    return Yhat - Y;
}

MatrixXd Loss::categoricalCrossEntropy(MatrixXd Yhat, MatrixXd Y) {
    return Yhat - Y;
}