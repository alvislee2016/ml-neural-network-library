#include "Metrics.hpp"

using namespace Eigen;
using namespace NNLibrary;

Metrics::Metrics() {}
Metrics::~Metrics() {}

double meanSquaredErrorScore(MatrixXd Yhat, MatrixXd Y) {
    return (Y - Yhat).array().pow(2).sum();
}
Metric Metrics::meanSquaredError { "MSE", meanSquaredErrorScore };

double rSquaredScore(MatrixXd Yhat, MatrixXd Y) {
    double numerator = (Y - Yhat).array().pow(2).sum();
    double denominator = (Y.array() - Y.mean()).pow(2).sum();
    return 1 - numerator / denominator;
}
Metric Metrics::rSquared { "R2", rSquaredScore };

double accuracyScore(MatrixXd Yhat, MatrixXd Y) {
    ArrayXd predict(Yhat.rows()), target(Y.rows());

    if(Yhat.cols() < 2) {
        predict = Yhat.array().round();
        target = Y.array();
    } else {
        for(int i = 0; i < Yhat.rows(); i++) {
            Yhat.row(i).maxCoeff(&predict[i]);
            Y.row(i).maxCoeff(&target[i]);
        }
    }
    
    double correct = (predict == target).count();
    return correct / target.rows();
}
Metric Metrics::accuracy { "Accuracy", accuracyScore };