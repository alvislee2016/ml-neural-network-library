#include <cmath>
#include "Layer/LinearLayer.hpp"

using namespace NNLibrary;
using namespace Eigen;

LinearLayer::LinearLayer(int M, int N, double l1, double l2) : l1(l1), l2(l2) {
    // Create weights using Glorot Uniform
    W = MatrixXd::Random(M, N) * sqrt(6.0 / (M + N));
    b = VectorXd::Zero(N);

    cachedGradW = ArrayXXd::Zero(M, N);
    cachedGradb = ArrayXd::Zero(N);
}

LinearLayer::~LinearLayer() {}

MatrixXd LinearLayer::forward(MatrixXd X) {
    cachedX = X;
    return (X * W).rowwise() + b.transpose();
}

MatrixXd LinearLayer::backward(MatrixXd gradZ) {

    // Calculate gradient w.r.t. W and b
    gradW = cachedX.transpose() * gradZ;
    gradb = gradZ.colwise().sum();

    // gradX is the gradZ for the previous layer
    return gradZ * W.transpose();
}

void LinearLayer::update(double lr, MatrixXd gradW, VectorXd gradb) {
    this->W -= lr * (gradW + (l1 * W.array().sign()).matrix() + l2 * 2 * W);
    this->b -= lr * (gradb + (l1 * b.array().sign()).matrix() + l2 * 2 * b);
}