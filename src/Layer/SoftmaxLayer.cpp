#include "Layer/SoftmaxLayer.hpp"

using namespace Eigen;
using namespace NNLibrary;

SoftmaxLayer::SoftmaxLayer() {}
SoftmaxLayer::~SoftmaxLayer() {}

MatrixXd SoftmaxLayer::forward(MatrixXd X) {
    ArrayXXd exp = X.array().exp();
    return (exp.colwise() / exp.rowwise().sum()).matrix();
}

MatrixXd SoftmaxLayer::backward(MatrixXd gradZ) {
    // The derivative of softmax is combined with Loss layer
    return gradZ;
}