#include <cmath>
#include "Layer/SigmoidLayer.hpp"

using namespace NNLibrary;
using namespace Eigen;

SigmoidLayer::SigmoidLayer() {}
SigmoidLayer::~SigmoidLayer() {}

double SigmoidLayer::sigmoid(double x) {
    return 1 / (1 + exp(-x));
}

MatrixXd SigmoidLayer::forward(MatrixXd X) {
    cachedX = X;
    return X.unaryExpr(&sigmoid);
}

MatrixXd SigmoidLayer::backward(Eigen::MatrixXd gradZ) {
    ArrayXXd Z = cachedX.unaryExpr(&sigmoid).array();
    ArrayXXd gradSigmoid = Z * (1 - Z);

    return (gradZ.array() * gradSigmoid).matrix();
}

