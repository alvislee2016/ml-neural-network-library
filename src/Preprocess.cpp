#include "Preprocess.hpp"

using namespace Eigen;
using namespace NNLibrary;

Preprocess::Preprocess() {}
Preprocess::~Preprocess() {}

void Preprocess::minmaxNormalize(MatrixXd& data) {
    RowVectorXd max = data.colwise().maxCoeff();
    RowVectorXd min = data.colwise().minCoeff();
    data = ((data.rowwise() - min).array().rowwise() / (max - min).array()) * 2 - 1;
}

void Preprocess::shuffle(MatrixXd& data) {
    PermutationMatrix<Dynamic, Dynamic> p(data.rows());
    p.setIdentity();
    std::random_shuffle(p.indices().data(), p.indices().data() + p.indices().size());

    data = p * data;
}

MatrixXd Preprocess::onehotEncoding(VectorXd Y, int K) {
    MatrixXd classes = MatrixXd::Identity(K, K);
    MatrixXd onehot(Y.rows(), K);
    for(int i = 0; i < Y.rows(); i++) {
        onehot.row(i) = classes.row(Y[i]);
    }

    return onehot;
}