#include <iostream>
#include "Model/Sequential.hpp"
#include "Layer/LinearLayer.hpp"

using namespace Eigen;

NNLibrary::Sequential::Sequential() {}

NNLibrary::Sequential::~Sequential() {
    // delete all Layer object on heap
    layers.clear();
    delete optimizer;
}

/*******************
* Private Functions
********************/

MatrixXd NNLibrary::Sequential::forward(MatrixXd X) {
    MatrixXd Yhat = X;
    for(Layer* layer : layers) {
        Yhat = layer->forward(Yhat);
    }
    return Yhat;
}

void NNLibrary::Sequential::backward(MatrixXd gradient) {
    MatrixXd gradZ = gradient;
    for(auto it = layers.rbegin(); it != layers.rend(); ++it) {
        gradZ = (*it)->backward(gradZ);
    }
}

void NNLibrary::Sequential::update() {
    for(Layer* layer : layers) {
        if(LinearLayer* linear = dynamic_cast<LinearLayer*>(layer)) {
            optimizer->optimize(linear);
        }
    }
}

/*******************
* Public Functions
********************/

void NNLibrary::Sequential::add(Layer* layer) {
    layers.push_back(layer);
}

void NNLibrary::Sequential::compileLoss(MatrixXd (*loss)(MatrixXd, MatrixXd)) {
    this->loss = loss;
}

void NNLibrary::Sequential::compileOptimizer(Optimizer* optimizer) {
    this->optimizer = optimizer;
}

void NNLibrary::Sequential::compileMetric(Metric* metric) {
    this->metric = metric;
}

void NNLibrary::Sequential::fit(MatrixXd X, MatrixXd Y, int epochs, int batchSize) {
    for(int epoch = 0; epoch < epochs; epoch++) {
        for(int batch = 0; batch < X.rows(); batch += batchSize) {
            MatrixXd Xbatch, Ybatch;
            if(batch + batchSize < X.rows()) {
                Xbatch = X.middleRows(batch, batchSize);
                Ybatch = Y.middleRows(batch, batchSize);
            } else {
                Xbatch = X.bottomRows(X.rows() - batch);
                Ybatch = Y.bottomRows(Y.rows() - batch);
            }

            MatrixXd Yhat = forward(Xbatch);
            std::cout << "--------------------------------------------" << std::endl;
            std::cout << this->metric->name << ": " << (*this->metric->score)(Yhat, Ybatch) << std::endl;
        
            backward((*loss)(Yhat, Ybatch));
            update();
        }
    }
}

MatrixXd NNLibrary::Sequential::predict(MatrixXd X) {
    return forward(X);
}
