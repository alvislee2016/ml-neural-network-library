#include "Optimizer/SGD.hpp"

using namespace Eigen;
using namespace NNLibrary;

SGD::SGD(double lr, double momentum, bool nesterov) : 
    lr(lr), momentum(momentum), nesterov(nesterov) {}
SGD::~SGD() {}

void SGD::optimize(LinearLayer* layer) {
    MatrixXd gradW = layer->gradW;
    VectorXd gradb = layer->gradb;

    // Momentum
    if(nesterov) {
        // Nesterov momentum
        layer->cachedGradW = momentum * layer->cachedGradW + gradW.array();
        layer->cachedGradb = momentum * layer->cachedGradb + gradb.array();
        gradW = (momentum * layer->cachedGradW + gradW.array()).matrix();
        gradb = (momentum * layer->cachedGradb + gradb.array()).matrix();
    } else {
        layer->cachedGradW = momentum * layer->cachedGradW + (1-momentum) * gradW.array();
        layer->cachedGradb = momentum * layer->cachedGradb + (1-momentum) * gradb.array();
        gradW = layer->cachedGradW.matrix();
        gradb = layer->cachedGradb.matrix();
    }
    
    layer->update(lr, gradW, gradb);
}
