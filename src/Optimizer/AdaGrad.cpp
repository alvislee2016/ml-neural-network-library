#include "Optimizer/AdaGrad.hpp"

using namespace Eigen;
using namespace NNLibrary;

AdaGrad::AdaGrad(double lr) : lr(lr) {}
AdaGrad::~AdaGrad() {}

const double AdaGrad::epsilon = 0.000000001;

void AdaGrad::optimize(LinearLayer* layer) {
    MatrixXd gradW = layer->gradW;
    VectorXd gradb = layer->gradb;

    // Accumulate square of gradient
    layer->cachedGradW += gradW.array().pow(2);
    layer->cachedGradb += gradb.array().pow(2);

    // Update parameters by lr/sqrt(cached + epsilon) * gradient
    gradW = (gradW.array() / (layer->cachedGradW).sqrt() + epsilon).matrix();
    gradb = (gradb.array() / (layer->cachedGradb).sqrt() + epsilon).matrix();
    
    layer->update(lr, gradW, gradb);
}