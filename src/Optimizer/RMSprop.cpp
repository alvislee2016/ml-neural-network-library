#include "Optimizer/RMSprop.hpp"

using namespace Eigen;
using namespace NNLibrary;

RMSprop::RMSprop(double lr, double rho) : lr(lr), rho(rho) {}
RMSprop::~RMSprop() {}

const double RMSprop::epsilon = 0.000000001;

void RMSprop::optimize(LinearLayer* layer) {

    MatrixXd gradW = layer->gradW;
    VectorXd gradb = layer->gradb;

    // Exponential moving average of squared gradient
    layer->cachedGradW = rho * layer->cachedGradW + (1-rho) * gradW.array().pow(2);
    layer->cachedGradb = rho * layer->cachedGradb + (1-rho) * gradb.array().pow(2);
    
    // Update parameters by lr/sqrt(cached + epsilon) * gradient
    gradW = (gradW.array() / (layer->cachedGradW).sqrt() + epsilon).matrix();
    gradb = (gradb.array() / (layer->cachedGradb).sqrt() + epsilon).matrix();
    
    layer->update(lr, gradW, gradb);
}