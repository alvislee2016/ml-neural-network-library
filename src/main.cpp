#include <Eigen/Dense>
#include "CSVReader.hpp"
#include "Preprocess.hpp"
#include "Model/Sequential.hpp"
#include "Layer/LinearLayer.hpp"
#include "Layer/SigmoidLayer.hpp"
#include "Layer/SoftmaxLayer.hpp"
#include "Optimizer/SGD.hpp"
#include "Optimizer/AdaGrad.hpp"
#include "Optimizer/RMSprop.hpp"
#include "Loss.hpp"
#include "Metrics.hpp"

using namespace Eigen;
using namespace NNLibrary;
using namespace std;

int main() {

  /*******************
  * Linear Regression
  ********************/
  
  // Create data
  int N = 250;
  MatrixXd data(N, 3);
  data.col(0) = VectorXd::LinSpaced(N, -100, 100);
  data.col(1) = VectorXd::LinSpaced(N, -50, 150) * -1;
  data.col(2) = VectorXd::LinSpaced(N, 0, 250);
  
  // Preprocess data
  Preprocess::shuffle(data);
  Preprocess::minmaxNormalize(data);
  MatrixXd X = data.leftCols(2);
  VectorXd Y = data.rightCols(1);
  
  // Create model
  NNLibrary::Sequential model;
  model.add(new LinearLayer(X.cols(), 1));
  model.compileLoss(Loss::meanSquaredError);
  model.compileOptimizer(new SGD(0.01, 0.9, true));
  model.compileMetric(&Metrics::meanSquaredError);

  // Train model
  model.fit(X, Y, 10, 50);
  
  // Evaluate model
  MatrixXd predicts = model.predict(X);
  cout << "R2: " << Metrics::rSquared.score(predicts, Y) << endl;
  

  /*********************
  * Logistic Regression
  **********************/
  /*
  // Create data
  CSVReader reader("../data/classification.csv", ',');
  MatrixXd raw = reader.getData(400, 3);
  MatrixXd data = raw.topRows(200);
  
  // Preprocess data
  Preprocess::shuffle(data);
  MatrixXd X = data.leftCols(2);
  Preprocess::minmaxNormalize(X);
  VectorXd Y = data.rightCols(1);

  // Create model
  NNLibrary::Sequential model;
  model.add(new LinearLayer(X.cols(), 1));
  model.add(new SigmoidLayer());
  model.compileLoss(Loss::binaryCrossEntropy);
  model.compileOptimizer(new SGD(0.01));
  model.compileMetric(&Metrics::accuracy);

  // Train model
  model.fit(X, Y, 2, 32);

  // Evaluate model
  MatrixXd Yhat = model.predict(X);
  cout << "Accuracy: " << Metrics::accuracy.score(Yhat, Y) << endl;
  */

  /****************************
  * Multi-class Classification
  *****************************/
  /*
  // Create data
  CSVReader reader("../data/classification.csv", ',');
  MatrixXd data = reader.getData(400, 3);

  // Preprocess data
  Preprocess::shuffle(data);
  MatrixXd X = data.leftCols(2);
  Preprocess::minmaxNormalize(X);
  VectorXd Y = data.rightCols(1);
  MatrixXd onehotY = Preprocess::onehotEncoding(Y, 4);

  // Create model
  NNLibrary::Sequential model;
  model.add(new LinearLayer(X.cols(), 4));
  model.add(new SoftmaxLayer());
  model.compileLoss(Loss::categoricalCrossEntropy);
  model.compileOptimizer(new SGD(0.01, 0.9, true));
  model.compileMetric(&Metrics::accuracy);
  
  // Train model
  model.fit(X, onehotY, 2, 400);
  
  // Evaluate model
  MatrixXd Yhat = model.predict(X);
  cout << "Accuracy: " << Metrics::accuracy.score(Yhat, onehotY) << endl;
  */
  
  return 0;
}
